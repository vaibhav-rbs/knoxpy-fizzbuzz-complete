fizzbuzz package
================

Submodules
----------

fizzbuzz.fizzbuzz module
------------------------

.. automodule:: fizzbuzz.fizzbuzz
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: fizzbuzz
    :members:
    :undoc-members:
    :show-inheritance:
